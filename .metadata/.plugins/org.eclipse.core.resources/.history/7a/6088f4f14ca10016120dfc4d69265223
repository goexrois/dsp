/* ###################################################################
**     Filename    : Events.c
**     Project     : TP1_ADC
**     Processor   : 56F8037
**     Component   : Events
**     Version     : Driver 01.03
**     Compiler    : CodeWarrior DSP C Compiler
**     Date/Time   : 2016-09-14, 16:59, # CodeGen: 0
**     Abstract    :
**         This is user's event module.
**         Put your event handler code here.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file Events.c
** @version 01.03
** @brief
**         This is user's event module.
**         Put your event handler code here.
*/         
/*!
**  @addtogroup Events_module Events module documentation
**  @{
*/         
/* MODULE Events */
#include "stdio.h"
#include <stdlib.h>
#include "Cpu.h"
#include "Events.h"

/* Global Variables */
int freq_buf[] = {8, 16, 22, 44, 48};
int Filter_FLAG = 0;
Frac16 sample_buf[51] = {0};
Frac16 output_buf[51] = {0} ;
unsigned int aux = 0 ; 
int freq_index = 0 ;
int sample_index = 0 ; 
unsigned int flag = 0 ;
int counter = 0;
int M = 51;
int filter_mode_index = 0; // 0 --> Pasabajos, 1 --> Pasaatos, 2 --> Pasabanda, 3 -- Rechazabanda

const Frac16 coefs[4][51] = {
		{ 
				0,      0,     -3,      0,     12,      0,    -33,      0,     72, //PASABAJO
        0,   -140,      0,    248,      0,   -417,      0,    674,      0,
    -1078,      0,   1772,      0,  -3279,      0,  10363,  16384,  10363,
        0,  -3279,      0,   1772,      0,  -1078,      0,    674,      0,
     -417,      0,    248,      0,   -140,      0,     72,      0,    -33,
        0,     12,      0,     -3,      0,      0
		} ,
        {
                0,      0,     -2,     -4,     -7,    -11,    -17,    -24,    -33, //PASAALTO
              -43,    -56,    -70,    -87,   -105,   -124,   -144,   -165,   -186,
             -206,   -225,   -243,   -258,   -270,   -279,   -285,  32481,   -285,
             -279,   -270,   -258,   -243,   -225,   -206,   -186,   -165,   -144,
             -124,   -105,    -87,    -70,    -56,    -43,    -33,    -24,    -17,
              -11,     -7,     -4,     -2,      0,      0
        } ,
        {
               0,      0,     -1,     -8,      5,    -32,     14,    -59,     -5, //PASABANDA
             -43,   -109,     62,   -316,    219,   -509,    232,   -423,   -186,
             206,  -1196,   1394,  -2607,   2760,  -3873,   3681,  28386,   3681,
           -3873,   2760,  -2607,   1394,  -1196,    206,   -186,   -423,    232,
            -509,    219,   -316,     62,   -109,    -43,     -5,    -59,     14,
             -32,      5,     -8,     -1,      0,      0
       } ,
       {
               0,      0,      0,     -1,     -2,     -4,     -6,     -9,    -12, //RECHAZABANDA
             -17,    -22,    -29,    -36,    -44,    -53,    -63,    -72,    -83,
             -93,   -102,   -111,   -118,   -124,   -129,   -132,  32767,   -132,
            -129,   -124,   -118,   -111,   -102,    -93,    -83,    -72,    -63,
             -53,    -44,    -36,    -29,    -22,    -17,    -12,     -9,     -6,
              -4,     -2,     -1,      0,      0,      0
       }
};

Frac16 w[51] = {0};
//Frac16 coefs[51] =  {
//		102,      0,      0,      0,    156,      0,      0,      0,    286,
//		        0,      0,      0,    522,      0,      0,      0,    949,      0,
//		        0,      0,   1963,      0,      0,      0,  10405,  16384,  10405,
//		        0,      0,      0,   1963,      0,      0,      0,    949,      0,
//		        0,      0,    522,      0,      0,      0,    286,      0,      0,
//		        0,    156,      0,      0,      0,    102
//};
//Frac16 coefs[51] = {
//       0,      4,      0,     53,      0,    158,      0,    104,    169,
//       0,   2005,      0,  16384,  16384,      0,   2005,      0,    169,
//     104,      0,    158,      0,     53,      0,      4,      0
//};
//Frac16 coefs[51] = {
//        0,      4,    -19,     53,   -103,    158,   -180,    104,    169,
//     -784,   2005,  -4677,  16384,  16384,  -4677,   2005,   -784,    169,
//      104,   -180,    158,   -103,     53,    -19,      4,      0
//};

//Frac16 coefs[51] = {
//	      0,     0,   0,    0,   0,    0,   0,    0,    0,
//	     0,   0,  0,  4,  0,  0,   0,   0,    0,
//	      0,   0,    0,   0,    0,   0,     0,    0
//	};
//Frac16 o_fir;
/* User includes (#include below this line is not maintained by Processor Expert) */

/*
** ===================================================================
**     Event       :  AD1_OnEnd (module Events)
**
**     Component   :  AD1 [ADC]
**     Description :
**         This event is called after the measurement (which consists
**         of <1 or more conversions>) is/are finished.
**         The event is available only when the <Interrupt
**         service/event> property is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void AD1_OnEnd(void)
{
	if(/*flag*/1)
		AD1_GetValue16(&aux);
	//	AD1_Measure(0);
	//	AD1_Stop();
	
}

/*
** ===================================================================
**     Event       :  Start_Stop_OnInterrupt (module Events)
**
**     Component   :  Start_Stop [ExtInt]
**     Description :
**         This event is called when an active signal edge/level has
**         occurred.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void Start_Stop_OnInterrupt(void)
{
	Play_LED_NegVal();
	// no tiene sentido xq calcularia el promedio una sola vez
	flag = ( flag )? 0 : 1 ;
	
}


/*
** ===================================================================
**     Event       :  Change_FREQ_OnInterrupt (module Events)
**
**     Component   :  Change_FREQ [ExtInt]
**     Description :
**         This event is called when an active signal edge/level has
**         occurred.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void Change_FREQ_OnInterrupt(void)
{
	LED_NegVal();
	freq_index = (freq_index+1)%5 ;
	TI1_SetFreqkHz(/*freq_buf[freq_index]*/8);
}


/*
** ===================================================================
**     Event       :  TI1_OnInterrupt (module Events)
**
**     Component   :  TI1 [TimerInt]
**     Description :
**         When a timer interrupt occurs this event is called (only
**         when the component is enabled - <Enable> and the events are
**         enabled - <EnableEvent>). This event is enabled only if a
**         <interrupt service/event> is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void TI1_OnInterrupt(void)
{

	Frac16 y = 2000;
	Frac16 x;
	Frac16 filter_out;
	/*unsigned int sum_measure ;
		  unsigned int average_measure ;
		  int i ;
		  for(i=0; i<512; i++){
			  sum_measure = sum_measure + sample_buf[i] ;
		  }
		  average_measure = sum_measure / 512 ;
		  //DA1_SetValue(&average_measure) ;
		   */

//	AD1_Measure(0);
//	//AD1_Start();
//	sample_buf[sample_index] = aux >> 4 ; 
//	//printf("sample: %d\n", sample_buf[sample_index]);
//	DA1_SetValue(&(sample_buf[sample_index])) ;
//	sample_index = (sample_index+1)%512 ;
	
	AD1_Measure(0);
	//AD1_Start();
	x = aux >> 4/* (counter == 0) ? 1 : 0 ;*/;
	counter++;
	filter_out = (Filter_FLAG) ? (fir( M, coefs[filter_mode_index], sample_buf, x))/*>>4*/ : x;
	
	DA1_SetValue( &filter_out );
	
	sample_index = (sample_index+1)%512 ;
	
//	AD1_Measure(0);
//	//AD1_Start();
////	sample_buf[sample_index] = aux >> 4 ;
//	sample_buf[0] = 1; 
//	//printf("sample: %d\n", sample_buf[sample_index]);
//	//o_fir = fir( M, coefs, w, sample_buf[sample_index] ) ;
//	conv(M, coefs, 512, sample_buf, output_buf) ;
//	DA1_SetValue( &(output_buf[511]) );
//	sample_index = (sample_index+1)%512 ;
	
	
}

void  conv(int M, Frac16* h, int L, Frac16 * x, Frac16 * y)
{
	 int n, m;
	 for (n = 0; n < L+M; n++){
		 for (y[n] = 0, m = max(0, n-L+1); m <= min(n, M); m++){
			 y[n] += h[m] * x[n-m];
		 }
	 }
}

/* M = filter order, h = filter, w = state, x = input sample */
Frac16 fir(int M, const Frac16* h, Frac16 *w, Frac16 x ){ 	/* Usage: y = fir(M, h, w, x); */
	int i, j;
	Frac16 y=0;
//	unsigned long al = 2147483648;
//	Frac16 as = (Frac16) (al>>16);

	sample_buf[0] = x;/* read input */
	// Shift
	for(y=mult(h[M],sample_buf[M]), i=M-1; i>=0; i--){
		sample_buf[i+1] = sample_buf[i];/* data shift instruction */
		y = add(y, mult(h[i],sample_buf[i])); /* MAC instruction */
	}
	
	// Mult ans Acum
//	for(j=0; j<M; j++){
//		y = add(y, mult(h[j],sample_buf[j]));
//	}
	
//	return (Frac16) (y>>15);
	return y;
//	
//	int i;
//	w[0] = x; /* read input */
//	for (y=h[M]*w[M], i=M-1; i>=0; i--) {
//	w[i+1] = w[i]; /* data shift instruction */
//	y += h[i] * w[i]; /* MAC instruction */
//	}
//	return y; 

}
/*
** ===================================================================
**     Event       :  FilterEnable_OnInterrupt (module Events)
**
**     Component   :  FilterEnable [ExtInt]
**     Description :
**         This event is called when an active signal edge/level has
**         occurred.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrlayupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void FilterEnable_OnInterrupt(void)
{
	LED_FiltroON_NegVal();
	Filter_FLAG = ~Filter_FLAG;
}


/*
** ===================================================================
**     Event       :  Filter_Mode_OnInterrupt (module Events)
**
**     Component   :  Filter_Mode [ExtInt]
**     Description :
**         This event is called when an active signal edge/level has
**         occurred.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void Filter_Mode_OnInterrupt(void)
{
  filter_mode_index = (filter_mode_index+1)%4;
  Filter_Mode_Indicator_PutVal(filter_mode_index);
}


/* END Events */

/*! 
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.3 [05.09]
**     for the Freescale 56800 series of microcontrollers.
**
** ###################################################################
*/
