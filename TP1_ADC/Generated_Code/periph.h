/** ###################################################################
**
**     (c) Freescale Semiconductor, Inc.
**     2004 All Rights Reserved
**
**
**     Copyright : 1997 - 2014 Freescale Semiconductor, Inc. 
**     All Rights Reserved.
**     
**     Redistribution and use in source and binary forms, with or without modification,
**     are permitted provided that the following conditions are met:
**     
**     o Redistributions of source code must retain the above copyright notice, this list
**       of conditions and the following disclaimer.
**     
**     o Redistributions in binary form must reproduce the above copyright notice, this
**       list of conditions and the following disclaimer in the documentation and/or
**       other materials provided with the distribution.
**     
**     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
**       contributors may be used to endorse or promote products derived from this
**       software without specific prior written permission.
**     
**     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
**     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
**     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
**     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
**     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
**     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
**     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
**     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
**     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**     
**     http: www.freescale.com
**     mail: support@freescale.com
** ###################################################################*/

/* File: periph.h */

#ifndef __PERIPH_H
#define __PERIPH_H

#include "port.h"


#ifdef __cplusplus
extern "C" {
#endif


/*******************************************************
* Routines for Peripheral Memory Access
********************************************************
*  Parameter usage:
*   Addr - architecture structure field
*   Mask - bit to manipulate
*   Data - 16 bit word to assign or access
********************************************************/

/*
inline void periphBitSet(register UWord16 Mask, register volatile UWord16 * Addr)
{
  register unsigned int Value;

  asm(move.w  x:(Addr+$1ffc00),Value);
  asm(or.w    Mask,Value);
  asm(move.w  Value,x:(Addr+$1ffc00));
}
*/

/*
inline void periphBitClear(register UWord16 Mask, register volatile UWord16 * Addr)
{
  register unsigned int Value;

  asm(move.w  x:(Addr+$1ffc00),Value);
  asm(not.w   Mask);
  asm(and.w   Mask,Value);
  asm(move.w  Value,x:(Addr+$1ffc00));
}
*/

/*
inline void periphBitChange(register UWord16 Mask, register volatile UWord16 * Addr)
{
  register unsigned int Value;

  asm(move.w  x:(Addr+$1ffc00),Value);
  asm(eor.w   Mask,Value);
  asm(move.w  Value,x:(Addr+$1ffc00));
}
*/

/*
inline bool periphBitTest(register UWord16 Mask, register volatile UWord16 * Addr)
{
  register unsigned int Value;

  asm(move.w  x:(Addr+$1ffc00),Value);

  if ((Value & Mask) == 0)
  {
    return false;
  }

  return true;
}
*/

/*
inline void periphMemWrite(register UWord16 Data, register volatile UWord16 * Addr)
{
  asm(move.w  Data,X:(Addr+$1ffc00));
}
*/

/*
inline UWord16 periphMemRead(register volatile UWord16 * Addr)
{
  register unsigned int Value;

  asm(move.w  X:(Addr+$1ffc00),Value);

  return Value;
}
*/

#ifdef __cplusplus
}
#endif

#endif
