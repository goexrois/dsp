/* ###################################################################
**     Filename    : Events.c
**     Project     : TP1_ADC
**     Processor   : 56F8037
**     Component   : Events
**     Version     : Driver 01.03
**     Compiler    : CodeWarrior DSP C Compiler
**     Date/Time   : 2016-09-14, 16:59, # CodeGen: 0
**     Abstract    :
**         This is user's event module.
**         Put your event handler code here.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file Events.c
** @version 01.03
** @brief
**         This is user's event module.
**         Put your event handler code here.
*/         
/*!
**  @addtogroup Events_module Events module documentation
**  @{
*/         
/* MODULE Events */
#include "stdio.h"
#include <stdlib.h>
#include "Cpu.h"
#include "Events.h"

/* Global Variables */
int freq_buf[] = {8, 16, 22, 44, 48};
int Filter_FLAG = 0;
Frac16 sample_buf[LONG_ARRAY] = {0};
Frac16 output_buf[LONG_ARRAY] = {0} ;
unsigned int aux = 0 ; 
int freq_index = 0 ;
int sample_index = 0 ; 
unsigned int flag = 0 ;
int counter = 0;
int M = LONG_ARRAY;
int filter_mode_index = 0; // 0 --> Pasabajos, 1 --> Pasaatos, 2 --> Pasabanda, 3 -- Rechazabanda
const Frac16 coef_pasaB[651] = {
	       15,    -48,     13,    -40,     -6,    -19,    -32,      2,    -52,
	       11,    -53,      0,    -37,    -25,    -13,    -50,      4,    -61,
	        3,    -53,    -16,    -30,    -44,     -7,    -64,      3,    -65,
	       -8,    -47,    -34,    -21,    -60,     -2,    -71,     -2,    -61,
	      -22,    -36,    -50,    -11,    -70,      0,    -71,    -11,    -51,
	      -37,    -22,    -63,     -2,    -74,     -1,    -63,    -21,    -36,
	      -50,     -8,    -70,      5,    -70,     -6,    -48,    -33,    -17,
	      -60,      6,    -71,      7,    -58,    -13,    -28,    -43,      3,
	      -64,     17,    -62,      6,    -38,    -21,     -4,    -49,     21,
	      -60,     23,    -46,      2,    -13,    -29,     20,    -51,     35,
	      -49,     25,    -23,     -4,     14,    -34,     41,    -45,     44,
	      -29,     22,      6,    -11,     42,    -34,     58,    -32,     47,
	       -3,     16,     36,    -15,     65,    -28,     69,    -11,     45,
	       27,      9,     65,    -16,     83,    -14,     71,     16,     37,
	       58,      2,     89,    -12,     92,      6,     65,     46,     26,
	       87,     -2,    105,     -1,     91,     31,     53,     76,     14,
	      109,     -2,    111,     16,     81,     59,     36,    102,      5,
	      121,      5,    105,     38,     62,     86,     18,    120,     -1,
	      121,     17,     87,     62,     37,    108,      0,    128,     -2,
	      108,     33,     59,     85,      9,    122,    -14,    121,      4,
	       81,     53,     24,    102,    -19,    123,    -22,     99,     15,
	       43,     71,    -15,    111,    -42,    109,    -23,     63,     30,
	       -3,     85,    -53,    106,    -58,     78,    -17,     14,     45,
	      -53,     89,    -86,     85,    -65,     32,     -6,    -44,     56,
	     -102,     80,   -109,     47,    -63,    -28,      8,   -106,     58,
	     -144,     54,   -121,     -8,    -52,    -97,     20,   -165,     48,
	     -174,     10,   -120,    -78,    -36,   -171,     25,   -216,     21,
	     -188,    -53,   -105,   -159,    -18,   -241,     18,   -251,    -26,
	     -184,   -133,    -80,   -245,     -4,   -300,     -6,   -265,    -95,
	     -160,   -226,    -48,   -328,      1,   -339,    -53,   -253,   -186,
	     -117,   -329,    -14,   -399,    -13,   -352,   -126,   -210,   -298,
	      -56,   -433,     15,   -448,    -51,   -327,   -232,   -132,   -431,
	       21,   -531,     31,   -463,   -128,   -251,   -381,     -9,   -589,
	      113,   -617,     21,   -424,   -266,    -94,   -604,    188,   -793,
	      233,   -684,    -42,   -277,   -544,    247,  -1020,    587,  -1157,
	      454,   -726,   -284,    285,  -1513,   1639,  -2890,   2922,  -3969,
	     3700,  28346,   3700,  -3969,   2922,  -2890,   1639,  -1513,    285,
	     -284,   -726,    454,  -1157,    587,  -1020,    247,   -544,   -277,
	      -42,   -684,    233,   -793,    188,   -604,    -94,   -266,   -424,
	       21,   -617,    113,   -589,     -9,   -381,   -251,   -128,   -463,
	       31,   -531,     21,   -431,   -132,   -232,   -327,    -51,   -448,
	       15,   -433,    -56,   -298,   -210,   -126,   -352,    -13,   -399,
	      -14,   -329,   -117,   -186,   -253,    -53,   -339,      1,   -328,
	      -48,   -226,   -160,    -95,   -265,     -6,   -300,     -4,   -245,
	      -80,   -133,   -184,    -26,   -251,     18,   -241,    -18,   -159,
	     -105,    -53,   -188,     21,   -216,     25,   -171,    -36,    -78,
	     -120,     10,   -174,     48,   -165,     20,    -97,    -52,     -8,
	     -121,     54,   -144,     58,   -106,      8,    -28,    -63,     47,
	     -109,     80,   -102,     56,    -44,     -6,     32,    -65,     85,
	      -86,     89,    -53,     45,     14,    -17,     78,    -58,    106,
	      -53,     85,     -3,     30,     63,    -23,    109,    -42,    111,
	      -15,     71,     43,     15,     99,    -22,    123,    -19,    102,
	       24,     53,     81,      4,    121,    -14,    122,      9,     85,
	       59,     33,    108,     -2,    128,      0,    108,     37,     62,
	       87,     17,    121,     -1,    120,     18,     86,     62,     38,
	      105,      5,    121,      5,    102,     36,     59,     81,     16,
	      111,     -2,    109,     14,     76,     53,     31,     91,     -1,
	      105,     -2,     87,     26,     46,     65,      6,     92,    -12,
	       89,      2,     58,     37,     16,     71,    -14,     83,    -16,
	       65,      9,     27,     45,    -11,     69,    -28,     65,    -15,
	       36,     16,     -3,     47,    -32,     58,    -34,     42,    -11,
	        6,     22,    -29,     44,    -45,     41,    -34,     14,     -4,
	      -23,     25,    -49,     35,    -51,     20,    -29,    -13,      2,
	      -46,     23,    -60,     21,    -49,     -4,    -21,    -38,      6,
	      -62,     17,    -64,      3,    -43,    -28,    -13,    -58,      7,
	      -71,      6,    -60,    -17,    -33,    -48,     -6,    -70,      5,
	      -70,     -8,    -50,    -36,    -21,    -63,     -1,    -74,     -2,
	      -63,    -22,    -37,    -51,    -11,    -71,      0,    -70,    -11,
	      -50,    -36,    -22,    -61,     -2,    -71,     -2,    -60,    -21,
	      -34,    -47,     -8,    -65,      3,    -64,     -7,    -44,    -30,
	      -16,    -53,      3,    -61,      4,    -50,    -13,    -25,    -37,
	        0,    -53,     11,    -52,      2,    -32,    -19,     -6,    -40,
	       13,    -48,     15
	};
const Frac16 coef_rechazaB[651] = {
	      -60,    -60,    -61,    -62,    -62,    -62,    -63,    -63,    -63,
	      -63,    -63,    -63,    -62,    -62,    -61,    -61,    -60,    -59,
	      -58,    -57,    -56,    -55,    -53,    -52,    -50,    -49,    -47,
	      -45,    -43,    -41,    -39,    -37,    -35,    -32,    -30,    -27,
	      -25,    -22,    -20,    -17,    -14,    -12,     -9,     -6,     -3,
	        0,      3,      6,      9,     12,     15,     18,     21,     24,
	       27,     30,     33,     36,     38,     41,     44,     47,     49,
	       52,     55,     57,     60,     62,     64,     66,     68,     70,
	       72,     74,     76,     77,     79,     80,     81,     82,     83,
	       84,     85,     86,     86,     87,     87,     87,     87,     87,
	       86,     86,     85,     84,     84,     82,     81,     80,     79,
	       77,     75,     74,     72,     70,     67,     65,     63,     60,
	       57,     55,     52,     49,     46,     43,     39,     36,     33,
	       29,     26,     22,     19,     15,     11,      8,      4,      0,
	       -4,     -8,    -12,    -15,    -19,    -23,    -27,    -31,    -34,
	      -38,    -42,    -45,    -49,    -53,    -56,    -59,    -63,    -66,
	      -69,    -72,    -75,    -78,    -81,    -83,    -86,    -88,    -90,
	      -92,    -94,    -96,    -98,    -99,   -101,   -102,   -103,   -104,
	     -105,   -106,   -106,   -106,   -106,   -106,   -106,   -106,   -105,
	     -105,   -104,   -103,   -101,   -100,    -99,    -97,    -95,    -93,
	      -91,    -89,    -86,    -84,    -81,    -78,    -75,    -72,    -69,
	      -65,    -62,    -58,    -55,    -51,    -47,    -43,    -39,    -35,
	      -31,    -26,    -22,    -18,    -13,     -9,     -4,      0,      4,
	        9,     13,     18,     22,     27,     31,     36,     40,     44,
	       48,     53,     57,     61,     65,     68,     72,     76,     79,
	       83,     86,     89,     92,     95,     98,    100,    103,    105,
	      107,    109,    111,    113,    114,    115,    116,    117,    118,
	      119,    119,    119,    119,    119,    119,    118,    118,    117,
	      116,    114,    113,    111,    109,    107,    105,    103,    100,
	       98,     95,     92,     89,     86,     82,     79,     75,     71,
	       68,     64,     60,     55,     51,     47,     42,     38,     33,
	       29,     24,     19,     14,     10,      5,      0,     -5,    -10,
	      -14,    -19,    -24,    -29,    -33,    -38,    -43,    -47,    -52,
	      -56,    -60,    -64,    -69,    -73,    -76,    -80,    -84,    -87,
	      -91,    -94,    -97,   -100,   -103,   -105,   -108,   -110,   -112,
	     -114,   -116,   -118,   -119,   -120,   -121,   -122,   -123,   -123,
	     -124,  32767,   -124,   -123,   -123,   -122,   -121,   -120,   -119,
	     -118,   -116,   -114,   -112,   -110,   -108,   -105,   -103,   -100,
	      -97,    -94,    -91,    -87,    -84,    -80,    -76,    -73,    -69,
	      -64,    -60,    -56,    -52,    -47,    -43,    -38,    -33,    -29,
	      -24,    -19,    -14,    -10,     -5,      0,      5,     10,     14,
	       19,     24,     29,     33,     38,     42,     47,     51,     55,
	       60,     64,     68,     71,     75,     79,     82,     86,     89,
	       92,     95,     98,    100,    103,    105,    107,    109,    111,
	      113,    114,    116,    117,    118,    118,    119,    119,    119,
	      119,    119,    119,    118,    117,    116,    115,    114,    113,
	      111,    109,    107,    105,    103,    100,     98,     95,     92,
	       89,     86,     83,     79,     76,     72,     68,     65,     61,
	       57,     53,     48,     44,     40,     36,     31,     27,     22,
	       18,     13,      9,      4,      0,     -4,     -9,    -13,    -18,
	      -22,    -26,    -31,    -35,    -39,    -43,    -47,    -51,    -55,
	      -58,    -62,    -65,    -69,    -72,    -75,    -78,    -81,    -84,
	      -86,    -89,    -91,    -93,    -95,    -97,    -99,   -100,   -101,
	     -103,   -104,   -105,   -105,   -106,   -106,   -106,   -106,   -106,
	     -106,   -106,   -105,   -104,   -103,   -102,   -101,    -99,    -98,
	      -96,    -94,    -92,    -90,    -88,    -86,    -83,    -81,    -78,
	      -75,    -72,    -69,    -66,    -63,    -59,    -56,    -53,    -49,
	      -45,    -42,    -38,    -34,    -31,    -27,    -23,    -19,    -15,
	      -12,     -8,     -4,      0,      4,      8,     11,     15,     19,
	       22,     26,     29,     33,     36,     39,     43,     46,     49,
	       52,     55,     57,     60,     63,     65,     67,     70,     72,
	       74,     75,     77,     79,     80,     81,     82,     84,     84,
	       85,     86,     86,     87,     87,     87,     87,     87,     86,
	       86,     85,     84,     83,     82,     81,     80,     79,     77,
	       76,     74,     72,     70,     68,     66,     64,     62,     60,
	       57,     55,     52,     49,     47,     44,     41,     38,     36,
	       33,     30,     27,     24,     21,     18,     15,     12,      9,
	        6,      3,      0,     -3,     -6,     -9,    -12,    -14,    -17,
	      -20,    -22,    -25,    -27,    -30,    -32,    -35,    -37,    -39,
	      -41,    -43,    -45,    -47,    -49,    -50,    -52,    -53,    -55,
	      -56,    -57,    -58,    -59,    -60,    -61,    -61,    -62,    -62,
	      -63,    -63,    -63,    -63,    -63,    -63,    -62,    -62,    -62,
	      -61,    -60,    -60
	};
const Frac16 coefs[4][LONG_ARRAY] = {
		{ 
				0,      0,     -3,      0,     12,      0,    -33,      0,     72, //PASABAJO
        0,   -140,      0,    248,      0,   -417,      0,    674,      0,
    -1078,      0,   1772,      0,  -3279,      0,  10363,  16384,  10363,
        0,  -3279,      0,   1772,      0,  -1078,      0,    674,      0,
     -417,      0,    248,      0,   -140,      0,     72,      0,    -33,
        0,     12,      0,     -3,      0,      0
		} ,
        {
                0,      0,     -2,     -4,     -7,    -11,    -17,    -24,    -33, //PASAALTO
              -43,    -56,    -70,    -87,   -105,   -124,   -144,   -165,   -186,
             -206,   -225,   -243,   -258,   -270,   -279,   -285,  32481,   -285,
             -279,   -270,   -258,   -243,   -225,   -206,   -186,   -165,   -144,
             -124,   -105,    -87,    -70,    -56,    -43,    -33,    -24,    -17,
              -11,     -7,     -4,     -2,      0,      0
        } ,
        {
               0,      0,     -1,     -8,      5,    -32,     14,    -59,     -5, //PASABANDA
             -43,   -109,     62,   -316,    219,   -509,    232,   -423,   -186,
             206,  -1196,   1394,  -2607,   2760,  -3873,   3681,  28386,   3681,
           -3873,   2760,  -2607,   1394,  -1196,    206,   -186,   -423,    232,
            -509,    219,   -316,     62,   -109,    -43,     -5,    -59,     14,
             -32,      5,     -8,     -1,      0,      0
       } ,
       {
               0,      0,      0,     -1,     -2,     -4,     -6,     -9,    -12, //RECHAZABANDA
             -17,    -22,    -29,    -36,    -44,    -53,    -63,    -72,    -83,
             -93,   -102,   -111,   -118,   -124,   -129,   -132,  32767,   -132,
            -129,   -124,   -118,   -111,   -102,    -93,    -83,    -72,    -63,
             -53,    -44,    -36,    -29,    -22,    -17,    -12,     -9,     -6,
              -4,     -2,     -1,      0,      0,      0
       }
};

Frac16 w[LONG_ARRAY] = {0};
//Frac16 coefs[51] =  {
//		102,      0,      0,      0,    156,      0,      0,      0,    286,
//		        0,      0,      0,    522,      0,      0,      0,    949,      0,
//		        0,      0,   1963,      0,      0,      0,  10405,  16384,  10405,
//		        0,      0,      0,   1963,      0,      0,      0,    949,      0,
//		        0,      0,    522,      0,      0,      0,    286,      0,      0,
//		        0,    156,      0,      0,      0,    102
//};
//Frac16 coefs[51] = {
//       0,      4,      0,     53,      0,    158,      0,    104,    169,
//       0,   2005,      0,  16384,  16384,      0,   2005,      0,    169,
//     104,      0,    158,      0,     53,      0,      4,      0
//};
//Frac16 coefs[51] = {
//        0,      4,    -19,     53,   -103,    158,   -180,    104,    169,
//     -784,   2005,  -4677,  16384,  16384,  -4677,   2005,   -784,    169,
//      104,   -180,    158,   -103,     53,    -19,      4,      0
//};

//Frac16 coefs[51] = {
//	      0,     0,   0,    0,   0,    0,   0,    0,    0,
//	     0,   0,  0,  4,  0,  0,   0,   0,    0,
//	      0,   0,    0,   0,    0,   0,     0,    0
//	};
//Frac16 o_fir;
/* User includes (#include below this line is not maintained by Processor Expert) */

/*
** ===================================================================
**     Event       :  AD1_OnEnd (module Events)
**
**     Component   :  AD1 [ADC]
**     Description :
**         This event is called after the measurement (which consists
**         of <1 or more conversions>) is/are finished.
**         The event is available only when the <Interrupt
**         service/event> property is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void AD1_OnEnd(void)
{
	if(/*flag*/1)
		AD1_GetValue16(&aux);
	//	AD1_Measure(0);
	//	AD1_Stop();
	
}

/*
** ===================================================================
**     Event       :  Start_Stop_OnInterrupt (module Events)
**
**     Component   :  Start_Stop [ExtInt]
**     Description :
**         This event is called when an active signal edge/level has
**         occurred.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void Start_Stop_OnInterrupt(void)
{
	Play_LED_NegVal();
	// no tiene sentido xq calcularia el promedio una sola vez
	flag = ( flag )? 0 : 1 ;
	
}


/*
** ===================================================================
**     Event       :  Change_FREQ_OnInterrupt (module Events)
**
**     Component   :  Change_FREQ [ExtInt]
**     Description :
**         This event is called when an active signal edge/level has
**         occurred.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void Change_FREQ_OnInterrupt(void)
{
	LED_NegVal();
	freq_index = (freq_index+1)%5 ;
	TI1_SetFreqkHz(/*freq_buf[freq_index]*/8);
}


/*
** ===================================================================
**     Event       :  TI1_OnInterrupt (module Events)
**
**     Component   :  TI1 [TimerInt]
**     Description :
**         When a timer interrupt occurs this event is called (only
**         when the component is enabled - <Enable> and the events are
**         enabled - <EnableEvent>). This event is enabled only if a
**         <interrupt service/event> is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void TI1_OnInterrupt(void)
{

	Frac16 y = 2000;
	Frac16 x;
	Frac16 filter_out;
	/*unsigned int sum_measure ;
		  unsigned int average_measure ;
		  int i ;
		  for(i=0; i<512; i++){
			  sum_measure = sum_measure + sample_buf[i] ;
		  }
		  average_measure = sum_measure / 512 ;
		  //DA1_SetValue(&average_measure) ;
		   */

//	AD1_Measure(0);
//	//AD1_Start();
//	sample_buf[sample_index] = aux >> 4 ; 
//	//printf("sample: %d\n", sample_buf[sample_index]);
//	DA1_SetValue(&(sample_buf[sample_index])) ;
//	sample_index = (sample_index+1)%512 ;
	
	AD1_Measure(0);
	//AD1_Start();
	x = aux >> 4/* (counter == 0) ? 1 : 0 ;*/;
	counter++;
	filter_out = (Filter_FLAG) ? (fir( M, /*coefs[filter_mode_index]*//*coef_rechazaB*/coef_pasaB, sample_buf, x))/*>>4*/ : x;
	
	DA1_SetValue( &filter_out );
	
	sample_index = (sample_index+1)%512 ;
	
//	AD1_Measure(0);
//	//AD1_Start();
////	sample_buf[sample_index] = aux >> 4 ;
//	sample_buf[0] = 1; 
//	//printf("sample: %d\n", sample_buf[sample_index]);
//	//o_fir = fir( M, coefs, w, sample_buf[sample_index] ) ;
//	conv(M, coefs, 512, sample_buf, output_buf) ;
//	DA1_SetValue( &(output_buf[511]) );
//	sample_index = (sample_index+1)%512 ;
	
	
}

void  conv(int M, Frac16* h, int L, Frac16 * x, Frac16 * y)
{
	 int n, m;
	 for (n = 0; n < L+M; n++){
		 for (y[n] = 0, m = max(0, n-L+1); m <= min(n, M); m++){
			 y[n] += h[m] * x[n-m];
		 }
	 }
}

/* M = filter order, h = filter, w = state, x = input sample */
Frac16 fir(int M, const Frac16* h, Frac16 *w, Frac16 x ){ 	/* Usage: y = fir(M, h, w, x); */
	int i, j;
	Frac16 y=0;
//	unsigned long al = 2147483648;
//	Frac16 as = (Frac16) (al>>16);

	sample_buf[0] = x;/* read input */
	// Shift
	for(y=mult(h[M-1], sample_buf[M-1]), i=M-2; i>=0; i--){
		sample_buf[i+1] = sample_buf[i];/* data shift instruction */
		y = add(y, mult(h[i],sample_buf[i])); /* MAC instruction */
//		y = mac_r(y, h[i], sample_buf[i]); /* MAC instruction */

	}
	
	// Mult ans Acum
//	for(j=0; j<M; j++){
//		y = add(y, mult(h[j],sample_buf[j]));
//	}
	
//	return (Frac16) (y>>15);
	return y;
//	
//	int i;
//	w[0] = x; /* read input */
//	for (y=h[M]*w[M], i=M-1; i>=0; i--) {
//	w[i+1] = w[i]; /* data shift instruction */
//	y += h[i] * w[i]; /* MAC instruction */
//	}
//	return y; 

}
/*
** ===================================================================
**     Event       :  FilterEnable_OnInterrupt (module Events)
**
**     Component   :  FilterEnable [ExtInt]
**     Description :
**         This event is called when an active signal edge/level has
**         occurred.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrlayupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void FilterEnable_OnInterrupt(void)
{
	LED_FiltroON_NegVal();
	Filter_FLAG = ~Filter_FLAG;
}


/*
** ===================================================================
**     Event       :  Filter_Mode_OnInterrupt (module Events)
**
**     Component   :  Filter_Mode [ExtInt]
**     Description :
**         This event is called when an active signal edge/level has
**         occurred.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
/* Comment following line if the appropriate 'Interrupt preserve registers' property */
/* is set to 'yes' (#pragma interrupt saveall is generated before the ISR)           */
#pragma interrupt called
void Filter_Mode_OnInterrupt(void)
{
  filter_mode_index = (filter_mode_index+1)%4;
  Filter_Mode_Indicator_PutVal(filter_mode_index);
}


/* END Events */

/*! 
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.3 [05.09]
**     for the Freescale 56800 series of microcontrollers.
**
** ###################################################################
*/
