/* ###################################################################
**     Filename    : main.c
**     Project     : TP1_ADC
**     Processor   : 56F8037
**     Version     : Driver 01.16
**     Compiler    : CodeWarrior DSP C Compiler
**     Date/Time   : 2016-09-14, 16:59, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.16
** @brief
**         Main module.
**         This module contains user's application code.
*/         
/*!
**  @addtogroup main_module main module documentation
**  @{
*/         
/* MODULE main */

/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "AD1.h"
#include "MFR1.h"
#include "FilterEnable.h"
#include "LED_FiltroON.h"
#include "Filter_Mode.h"
#include "Filter_Mode_Indicator.h"
#include "DA1.h"
#include "TI1.h"
#include "Change_FREQ.h"
#include "Start_Stop.h"
#include "LED.h"
#include "Play_LED.h"
/* Including shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"

//int sample_index;
//unsigned int sample_buf[512];

void main(void)
{
  /* Write your local variable definition here */
  
  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/
  /* Write your code here */
  for(;;)
  { 
	  
  }
  for(;;) {}
}

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.3 [05.09]
**     for the Freescale 56800 series of microcontrollers.
**
** ###################################################################
*/
