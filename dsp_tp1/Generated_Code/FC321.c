/* ###################################################################
**     THIS COMPONENT MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename    : FC321.c
**     Project     : dsp_tp1
**     Processor   : 56F8037
**     Component   : FreeCntr32
**     Version     : Component 02.070, Driver 02.05, CPU db: 2.87.264
**     Compiler    : CodeWarrior DSP C Compiler
**     Date/Time   : 2016-09-08, 18:05, # CodeGen: 1
**     Abstract    :
**         This device "FreeCntr32" implements 32-bit Free Running Counter
**     Settings    :
**         Timer name                  : TMRA0 (16-bit)
**         Compare name                : TMRA0_Compare
**         Counter shared              : No
**
**         High speed mode
**             Prescaler               : divide-by-1
**             Clock                   : 32000000 Hz
**           Resolution of timer
**             Xtal ticks              : 8000
**             microseconds            : 1000
**             milliseconds            : 1
**             seconds (real)          : 0.001
**             Hz                      : 1000
**             kHz                     : 1
**
**         Initialization:
**              Timer                  : Enabled
**
**         Timer registers
**              Counter                : TMRA0_CNTR [F005]
**              Mode                   : TMRA0_CTRL [F006]
**              Run                    : TMRA0_CTRL [F006]
**              Prescaler              : TMRA0_CTRL [F006]
**
**         Compare registers
**              Compare                : TMRA0_COMP1 [F000]
**     Contents    :
**         Enable    - byte FC321_Enable(void);
**         Disable   - byte FC321_Disable(void);
**         Reset     - byte FC321_Reset(void);
**         GetTimeUS - byte FC321_GetTimeUS(word *Time);
**         GetTimeMS - byte FC321_GetTimeMS(word *Time);
**
**     Copyright : 1997 - 2014 Freescale Semiconductor, Inc. 
**     All Rights Reserved.
**     
**     Redistribution and use in source and binary forms, with or without modification,
**     are permitted provided that the following conditions are met:
**     
**     o Redistributions of source code must retain the above copyright notice, this list
**       of conditions and the following disclaimer.
**     
**     o Redistributions in binary form must reproduce the above copyright notice, this
**       list of conditions and the following disclaimer in the documentation and/or
**       other materials provided with the distribution.
**     
**     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
**       contributors may be used to endorse or promote products derived from this
**       software without specific prior written permission.
**     
**     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
**     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
**     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
**     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
**     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
**     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
**     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
**     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
**     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**     
**     http: www.freescale.com
**     mail: support@freescale.com
** ###################################################################*/
/*!
** @file FC321.c
** @version 02.05
** @brief
**         This device "FreeCntr32" implements 32-bit Free Running Counter
*/         
/*!
**  @addtogroup FC321_module FC321 module documentation
**  @{
*/         

/* MODULE FC321. */

#include "FC321.h"

static bool EnUser;                    /* Enable/Disable device by user */
static dword TTicks;                   /* Counter of timer ticks */
static dword LTicks;                   /* Working copy of variable TTicks */
static bool TOvf;                      /* Counter overflow flag */
static bool LOvf;                      /* Working copy of variable TOvf */

/* Internal method prototypes */
static void HWEnDi(void);
static void LoadTicks(void);
static void SetCV(word Val);
static void SetPV(byte Val);

/*
** ===================================================================
**     Method      :  SetCV (component FreeCntr32)
**
**     Description :
**         Sets compare or preload register value. The method is called 
**         automatically as a part of several internal methods.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
static void SetCV(word Val)
{
  EnterCritical();                     /* Disable global interrupts */
  setReg(TMRA0_CMPLD1,Val);            /* Store given value to the compare preload 1 register */
  setReg(TMRA0_CMPLD2,Val);            /* Store given value to the compare preload 2 register */
  ExitCritical();                      /* Enable global interrupts */
}

/*
** ===================================================================
**     Method      :  SetPV (component FreeCntr32)
**
**     Description :
**         Sets prescaler value. The method is called automatically as a 
**         part of several internal methods.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
static void SetPV(byte Val)
{
  setRegBitGroup(TMRA0_CTRL,PCS,Val);  /* Store given value to the prescaler */
  setReg(TMRA0_CNTR,0);                /* Reset counter */
}

/*
** ===================================================================
**     Method      :  LoadTicks (component FreeCntr32)
**
**     Description :
**         Loads actual number of timer ticks from internal variable. The 
**         method is called automatically as a part of several internal 
**         methods.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
static void LoadTicks(void)
{
  EnterCritical();                     /* Disable global interrupts */
  LTicks = TTicks;                     /* Loading actual number of timer ticks */
  LOvf = TOvf;                         /* Loading actual state of "overflow flag" */
  ExitCritical();                      /* Enable global interrupts */
}

/*
** ===================================================================
**     Method      :  HWEnDi (component FreeCntr32)
**
**     Description :
**         Enables or disables the peripheral(s) associated with the bean.
**         The method is called automatically as a part of the Enable and 
**         Disable methods and several internal methods.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
static void HWEnDi(void)
{
  if (EnUser) {                        /* Enable device? */
    setReg(TMRA0_COMP1,getReg(TMRA0_CMPLD1)); /* Refresh compare 1 register */
    setReg(TMRA0_COMP2,getReg(TMRA0_CMPLD2)); /* Refresh compare 2 register */
    setRegBitGroup(TMRA0_CTRL,CM,0x01); /* Run counter */
  }
  else {                               /* Disable device? */
    setRegBitGroup(TMRA0_CTRL,CM,0x00); /* Stop counter */
    setReg(TMRA0_CNTR,0);              /* Reset counter */
  }
}

/*
** ===================================================================
**     Method      :  FC321_Enable (component FreeCntr32)
*/
/*!
**     @brief
**         This method enables the component - the internal clocks are
**         counted.
**     @return
**                         - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
*/
/* ===================================================================*/
byte FC321_Enable(void)
{
  if (!EnUser) {                       /* Is the device disabled by user? */
    EnUser = TRUE;                     /* If yes then set the flag "device enabled" */
    HWEnDi();                          /* Enable the device */
  }
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  FC321_Disable (component FreeCntr32)
*/
/*!
**     @brief
**         This method disables the component - the internal clocks are
**         not counted.
**     @return
**                         - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
*/
/* ===================================================================*/
byte FC321_Disable(void)
{
  if (EnUser) {                        /* Is the device enabled by user? */
    EnUser = FALSE;                    /* If yes then set the flag "device disabled" */
    HWEnDi();                          /* Disable the device */
  }
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  FC321_Reset (component FreeCntr32)
*/
/*!
**     @brief
**         This method clears the counter.
**     @return
**                         - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
*/
/* ===================================================================*/
byte FC321_Reset(void)
{
  EnterCritical();                     /* Disable global interrupts */
  setReg(TMRA0_CNTR,0x00);             /* Reset counter register */
  TTicks =  0U;                        /* Reset counter of timer ticks */
  TOvf = FALSE;                        /* Reset counter overflow flag */
  ExitCritical();                      /* Enable global interrupts */
  return ERR_OK;                       /* OK */
}

/*
** ===================================================================
**     Method      :  FC321_GetTimeUS (component FreeCntr32)
**     Description :
**         Returns the time (as a 16-bit unsigned integer) in microseconds
**         since the last resetting after the last reset.
**     Parameters  :
**         NAME            - DESCRIPTION
**       * Time            - A pointer to the returned 16-bit value
**                           in microseconds
**     Returns     :
**         ---             - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_OVERFLOW - Software counter overflow
**                           ERR_MATH - Overflow during evaluation
** ===================================================================
*/
byte FC321_GetTimeUS(word *Time)
{
  dlong rtval;                         /* Result of two 32-bit numbers multiplication */

  LoadTicks();                         /* Load actual state of counter */
  if (LOvf) {                          /* Testing counter overflow */
    return ERR_OVERFLOW;               /* If yes then error */
  }
  PE_Timer_LngMul(LTicks,0x03E80000UL,&rtval); /* Multiply ticks and high speed CPU mode coefficient */
  if (PE_Timer_LngHi2(rtval[0],rtval[1],Time)) { /* Is the result greater or equal than 65536 ? */
    return ERR_MATH;                   /* If yes then error */
  }
  else {                               /* Is the result less than 65536 ? */
    return ERR_OK;                     /* If yes then OK */
  }
}

/*
** ===================================================================
**     Method      :  FC321_GetTimeMS (component FreeCntr32)
**     Description :
**         Returns the time (as a 16-bit unsigned integer) in milliseconds
**         since the last resetting after the last reset.
**     Parameters  :
**         NAME            - DESCRIPTION
**       * Time            - A pointer to the returned 16-bit value
**                           in milliseconds
**     Returns     :
**         ---             - Error code, possible codes:
**                           ERR_OK - OK
**                           ERR_SPEED - This device does not work in
**                           the active speed mode
**                           ERR_OVERFLOW - Software counter overflow
**                           ERR_MATH - Overflow during evaluation
** ===================================================================
*/
byte FC321_GetTimeMS(word *Time)
{
  dlong rtval;                         /* Result of two 32-bit numbers multiplication */

  LoadTicks();                         /* Load actual state of counter */
  if (LOvf) {                          /* Testing counter overflow */
    return ERR_OVERFLOW;               /* If yes then error */
  }
  PE_Timer_LngMul(LTicks,0x01000000UL,&rtval); /* Multiply ticks and high speed CPU mode coefficient */
  if (PE_Timer_LngHi3(rtval[0],rtval[1],Time)) { /* Is the result greater or equal than 65536 ? */
    return ERR_MATH;                   /* If yes then error */
  }
  else {                               /* Is the result less than 65536 ? */
    return ERR_OK;                     /* If yes then OK */
  }
}

/*
** ===================================================================
**     Method      :  FC321_Init (component FreeCntr32)
**
**     Description :
**         Initializes the associated peripheral(s) and the beans 
**         internal variables. The method is called automatically as a 
**         part of the application initialization code.
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
void FC321_Init(void)
{
  /* TMRA0_CTRL: CM=0,PCS=0,SCS=0,ONCE=0,LENGTH=1,DIR=0,Co_INIT=0,OM=4 */
  setReg(TMRA0_CTRL,0x24);             /* Stop timer, use alternating compare registers */
  /* TMRA0_SCTRL: TCF=0,TCFIE=1,TOF=0,TOFIE=0,IEF=0,IEFIE=0,IPS=0,INPUT=0,Capture_Mode=0,MSTR=0,EEOF=0,VAL=0,FORCE=0,OPS=0,OEN=0 */
  setReg(TMRA0_SCTRL,0x4000);
  setReg(TMRA0_CNTR,0x00);             /* Reset counter register */
  setReg(TMRA0_LOAD,0x00);             /* Reset load register */
  setReg(TMRA0_COMP1,0x7CFF);          /* Set up compare 1 register */
  setReg(TMRA0_COMP2,0x7CFF);          /* Set up compare 2 register */
  /* TMRA0_CSCTRL: DBG_EN=0,??=0,??=0,??=0,??=0,??=0,??=0,TCF2EN=0,TCF1EN=0,TCF2=0,TCF1=0,CL2=1,CL1=2 */
  setReg(TMRA0_CSCTRL,0x06);           /* Enable compare 1 interrupt */
  TTicks = 0U;                         /* Counter of timer ticks */
  TOvf = FALSE;                        /* Counter overflow flag */
  EnUser = TRUE;                       /* Enable device */
  SetCV((word)0x7CFF);                 /* Store appropriate value to the compare register according to the selected high speed CPU mode */
  SetPV((byte)0x08);                   /* Set prescaler register according to the selected high speed CPU mode */
  HWEnDi();                            /* Enable/disable device according to status flags */
}

/*
** ===================================================================
**     Method      :  FC321_Interrupt (component FreeCntr32)
**
**     Description :
**         The method services the interrupt of the selected peripheral(s)
**         and eventually invokes the beans event(s).
**         This method is internal. It is used by Processor Expert only.
** ===================================================================
*/
#pragma interrupt alignsp 
void FC321_Interrupt(void)
{
  clrRegBit(TMRA0_SCTRL,TCF);          /* Reset interrupt request flag */
  TTicks++;                            /* Increment counter of timer ticks */
  if (TTicks == 0U) {                  /* Testing counter overflow */
    TOvf = TRUE;                       /* If yes then set overflow flag */
  }
}

/* END FC321. */

/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.3 [05.09]
**     for the Freescale 56800 series of microcontrollers.
**
** ###################################################################
*/
