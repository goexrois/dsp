/** ###################################################################
**     THIS COMPONENT MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename    : Vectors.c
**     Project     : dsp_tp1
**     Processor   : 56F8037
**     Version     : Component 01.072, Driver 02.10, CPU db: 2.87.264
**     Compiler    : CodeWarrior DSP C Compiler
**     Date/Time   : 2016-09-08, 18:05, # CodeGen: 1
**     Abstract    :
**
**     Settings    :
**
**
**     Copyright : 1997 - 2014 Freescale Semiconductor, Inc. 
**     All Rights Reserved.
**     
**     Redistribution and use in source and binary forms, with or without modification,
**     are permitted provided that the following conditions are met:
**     
**     o Redistributions of source code must retain the above copyright notice, this list
**       of conditions and the following disclaimer.
**     
**     o Redistributions in binary form must reproduce the above copyright notice, this
**       list of conditions and the following disclaimer in the documentation and/or
**       other materials provided with the distribution.
**     
**     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
**       contributors may be used to endorse or promote products derived from this
**       software without specific prior written permission.
**     
**     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
**     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
**     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
**     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
**     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
**     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
**     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
**     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
**     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**     
**     http: www.freescale.com
**     mail: support@freescale.com
** ###################################################################*/
/*!
** @file Vectors.c                                                  
** @version 02.10
** @brief
**
*/         
/*!
**  @addtogroup Vectors_module Vectors module documentation
**  @{
*/         

#include "Cpu.h"
#include "FC321.h"
#include "Bit1.h"
#include "SW1.h"
#include "SW2.h"
#include "AD1.h"
#include "DA1.h"
#include "Events.h"

#ifndef _lint

extern void _EntryPoint(void);         /* Startup routine */

volatile asm void _vectboot(void);
#pragma define_section interrupt_vectorsboot "interrupt_vectorsboot.text"  RX
#pragma section interrupt_vectorsboot begin
volatile asm void _vectboot(void) {
  JMP  _EntryPoint                     /* Reset vector (Used) */
  JMP  _EntryPoint                     /* COP reset vector (Used) */
}
#pragma section interrupt_vectorsboot end

volatile asm void _vect(void);
#pragma define_section interrupt_vectors "interrupt_vectors.text"  RX
#pragma section interrupt_vectors begin
volatile asm void _vect(void) {
  JMP  _EntryPoint                     /* Interrupt no. 0 (Used)   - ivINT_Reset */
  JMP  _EntryPoint                     /* Interrupt no. 1 (Used)   - ivINT_COPReset  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 2 (Unused) - ivINT_Illegal_Instruction  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 3 (Unused) - ivINT_SW3  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 4 (Unused) - ivINT_HWStackOverflow  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 5 (Unused) - ivINT_MisalignedLongWordAccess  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 6 (Unused) - ivINT_OnCE_StepCounter  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 7 (Unused) - ivINT_OnCE_BreakpointUnit  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 8 (Unused) - ivINT_OnCE_TraceBuffer  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 9 (Unused) - ivINT_OnCE_TxREmpty  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 10 (Unused) - ivINT_OnCE_RxRFull  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 11 (Unused) - ivINT_SW2  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 12 (Unused) - ivINT_SW1  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 13 (Unused) - ivINT_SW0  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 14 (Unused) - ivReserved0  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 15 (Unused) - ivINT_LVI  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 16 (Unused) - ivINT_PLL  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 17 (Unused) - ivINT_HFM_ERR  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 18 (Unused) - ivINT_HFM_CC  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 19 (Unused) - ivINT_HFM_CBE  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 20 (Unused) - ivINT_MSCAN_Error  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 21 (Unused) - ivINT_MSCAN_Rx  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 22 (Unused) - ivINT_MSCAN_Tx  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 23 (Unused) - ivINT_MSCAN_WakeUp  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 24 (Unused) - ivINT_GPIO_D  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 25 (Unused) - ivINT_GPIO_C  */
  JSR  Cpu__ivINT_GPIO_B               /* Interrupt no. 26 (Used)   - ivINT_GPIO_B  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 27 (Unused) - ivINT_GPIO_A  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 28 (Unused) - ivINT_QSPI0_RxFull  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 29 (Unused) - ivINT_QSPI0_TxEmpty  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 30 (Unused) - ivINT_QSPI1_RxFull  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 31 (Unused) - ivINT_QSPI1_TxEmpty  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 32 (Unused) - ivINT_QSCI0_TxEmpty  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 33 (Unused) - ivINT_QSCI0_TxIdle  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 34 (Unused) - ivINT_QSCI0_RxError  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 35 (Unused) - ivINT_QSCI0_RxFull  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 36 (Unused) - ivINT_QSCI1_TxEmpty  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 37 (Unused) - ivINT_QSCI1_TxIdle  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 38 (Unused) - ivINT_QSCI1_RxError  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 39 (Unused) - ivINT_QSCI1_RxFull  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 40 (Unused) - ivINT_I2C_Error  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 41 (Unused) - ivINT_I2C_General  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 42 (Unused) - ivINT_I2C_Rx  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 43 (Unused) - ivINT_I2C_Tx  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 44 (Unused) - ivINT_I2C_Status  */
  JSR  FC321_Interrupt                 /* Interrupt no. 45 (Used)   - ivINT_TMRA0  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 46 (Unused) - ivINT_TMRA1  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 47 (Unused) - ivINT_TMRA2  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 48 (Unused) - ivINT_TMRA3  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 49 (Unused) - ivINT_TMRB0  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 50 (Unused) - ivINT_TMRB1  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 51 (Unused) - ivINT_TMRB2  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 52 (Unused) - ivINT_TMRB3  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 53 (Unused) - ivINT_CMPA  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 54 (Unused) - ivINT_CMPB  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 55 (Unused) - ivINT_PIT0  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 56 (Unused) - ivINT_PIT1  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 57 (Unused) - ivINT_PIT2  */
  JSR  AD1_InterruptCC                 /* Interrupt no. 58 (Used)   - ivINT_ADCA_Complete  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 59 (Unused) - ivINT_ADCB_Complete  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 60 (Unused) - ivINT_ADC_ZC_LE  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 61 (Unused) - ivINT_PWM_Reload  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 62 (Unused) - ivINT_PWM_Fault  */
  JSR  Cpu_Interrupt                   /* Interrupt no. 63 (Unused) - ivINT_LP  */
}
#pragma section interrupt_vectors end

#endif _lint

/* Disable MISRA rules for interurupt routines */
/*lint -esym(765,Cpu_Interrupt) Disable MISRA rule (8.10) checking for symbols (Cpu_Interrupt). */
/*lint -esym(765,Cpu__ivINT_GPIO_B) Disable MISRA rule (8.10) checking for symbols (Cpu__ivINT_GPIO_B). */
/*lint -esym(765,FC321_Interrupt) Disable MISRA rule (8.10) checking for symbols (FC321_Interrupt). */
/*lint -esym(765,AD1_InterruptCC) Disable MISRA rule (8.10) checking for symbols (AD1_InterruptCC). */

/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.3 [05.09]
**     for the Freescale 56800 series of microcontrollers.
**
** ###################################################################
*/
